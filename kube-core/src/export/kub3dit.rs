use std::convert::TryFrom;
use std::io::{self, Seek, Write};

use flate2::{write::ZlibEncoder, Compression};
use image::{DynamicImage, ImageError, ImageFormat, RgbImage};

use crate::block::Block;
use crate::minimap::{AxisOrientation, HeightMap};
use crate::region::{self, Dimensions, RegionStorage};

pub const MAX_EXPORT_SIZE: usize = 50 * region::CHUNK_WIDTH;
const KUB3DIT_FORMAT_VERSION: u8 = 1;
const DEFAULT_IMAGE_BYTES: &[u8] = include_bytes!("kub3dit-default-image.png");

pub struct ExportOptions {
    pub kind: ExportKind,
}

pub enum ExportKind {
    Raw,
    WithMinimap,
    WithIcon,
}

pub fn export<D: Dimensions>(
    mut writer: impl Write + Seek,
    region: &RegionStorage<Block, D>,
    options: &ExportOptions,
) -> io::Result<()> {
    let dims = region.dims();
    if dims.width() > MAX_EXPORT_SIZE || dims.height() > MAX_EXPORT_SIZE {
        io::Error::new(
            io::ErrorKind::Other,
            format!(
                "can't export region: the maximum size is {}x{}",
                MAX_EXPORT_SIZE, MAX_EXPORT_SIZE
            ),
        );
    }

    let add_footer = match options.kind {
        ExportKind::Raw => false,
        ExportKind::WithMinimap => {
            let mut image = RgbImage::new(dims.width() as u32, dims.depth() as u32);
            let heightmap = HeightMap::from_region(region);
            let orientation = AxisOrientation {
                x_inverted: false,
                y_inverted: false,
            };

            // TODO: progressive rendering, a scan-line at a time?
            heightmap.render(&mut image, 0, 0, orientation);
            DynamicImage::ImageRgb8(image)
                .write_to(&mut writer, ImageFormat::Png)
                .map_err(|e| match e {
                    ImageError::IoError(err) => err,
                    err => io::Error::new(io::ErrorKind::Other, err),
                })?;
            true
        }
        ExportKind::WithIcon => {
            writer.write_all(DEFAULT_IMAGE_BYTES)?;
            true
        }
    };

    let mut compressed = ZlibEncoder::new(CountWrite::new(writer), Compression::best());
    write_map_data(&mut compressed, region)?;
    let mut writer = compressed.finish()?;
    if add_footer {
        writer.inner.write_all(&(writer.written).to_be_bytes())?;
        writer.inner.write_all(b".K3D")?;
    }
    writer.flush()?;
    Ok(())
}

fn write_map_data<D: Dimensions>(
    writer: &mut impl Write,
    region: &RegionStorage<Block, D>,
) -> io::Result<()> {
    writer.write_all(&[KUB3DIT_FORMAT_VERSION])?;

    // region dimensions
    fn to_be_u16(n: usize) -> [u8; 2] {
        u16::try_from(n).expect("dimension too big").to_be_bytes()
    }
    let dims = region.dims();
    writer.write_all(&to_be_u16(dims.width()))?;
    writer.write_all(&to_be_u16(dims.depth()))?;
    writer.write_all(&to_be_u16(dims.height().saturating_sub(1)))?;

    // Don't save z=0
    let bytes = if dims.height() == 0 {
        region.as_bytes()
    } else {
        let start = dims.width() * dims.depth();
        &region.as_bytes()[start..]
    };

    // Map data, replacing unknown blocks by air.
    let unknown_block: u8 = Block::Unknown.into();
    let air_block: &[u8] = &[Block::Empty.into()];

    let mut valid_runs = bytes.split(|b| *b == unknown_block);
    if let Some(first) = valid_runs.next() {
        writer.write_all(first)?;
    }
    for valid_run in valid_runs {
        writer.write_all(air_block)?;
        writer.write_all(valid_run)?;
    }
    Ok(())
}

struct CountWrite<W> {
    inner: W,
    // Not `usize`: we need a fixed byte length to match the kub3dit format.
    written: u32,
}

impl<W: Write> CountWrite<W> {
    fn new(inner: W) -> Self {
        Self { inner, written: 0 }
    }
}

impl<W: Write> Write for CountWrite<W> {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        let written = self.inner.write(buf)?;
        self.written = u32::try_from(written)
            .ok()
            .and_then(|w| self.written.checked_add(w))
            .ok_or_else(|| io::Error::new(io::ErrorKind::Other, "too much written data"))?;
        Ok(written)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.inner.flush()
    }
}
