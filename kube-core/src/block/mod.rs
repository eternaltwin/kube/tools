use std::ops::{Index, IndexMut};

use image::Rgb;

#[macro_use]
mod defs;

expand_all_blocks! {
    prefix = {
        #[derive(Debug, Copy, Clone, Eq, PartialEq)]
        #[repr(u8)]
        pub enum Block
    }
    repeat = braces {
        #NAME = #ID,
    }
    postfix = {}

}

pub type BlockTable<T> = [T; Block::NB_BLOCKS];

impl Default for Block {
    #[inline(always)]
    fn default() -> Self {
        Self::Empty
    }
}

impl From<u8> for Block {
    #[inline(always)]
    fn from(b: u8) -> Self {
        if b < Self::NB_BLOCKS as u8 {
            // SAFETY: We just checked that it is a valid block
            unsafe { Self::from_unchecked(b) }
        } else {
            Self::Unknown
        }
    }
}

impl From<Block> for u8 {
    #[inline(always)]
    fn from(b: Block) -> Self {
        b as u8
    }
}

impl Block {
    pub const NB_BLOCKS: usize = Block::Unknown as usize + 1;

    #[inline(always)]
    pub unsafe fn from_unchecked(b: u8) -> Self {
        std::mem::transmute::<u8, Self>(b)
    }

    pub fn from_bytes(mut bytes: Vec<u8>) -> Vec<Self> {
        // Sanitize unknown bytes.
        for b in &mut bytes {
            *b = Block::from(*b).into();
        }

        // SAFETY: Self is repr(u8), and the data has just been sanitized.
        unsafe { transmute_vec(bytes) }
    }

    pub fn into_bytes(blocks: Vec<Self>) -> Vec<u8> {
        // SAFETY: Self is repr(u8), and is always a valid u8 value.
        unsafe { transmute_vec(blocks) }
    }

    #[inline]
    pub fn as_bytes(blocks: &[Self]) -> &[u8] {
        // SAFETY: Self is repr(u8), and is always a valid u8 value.
        unsafe { std::slice::from_raw_parts(blocks.as_ptr() as *mut u8, blocks.len()) }
    }

    pub fn all() -> impl Iterator<Item = Block> {
        // SAFETY: All the bytes in the range are valid blocks.
        unsafe { (0..Self::NB_BLOCKS).map(|i| Self::from_unchecked(i as u8)) }
    }

    #[inline]
    pub fn map_color(self) -> BlockColor {
        use BlockColor::*;

        expand_all_blocks! {
            prefix = {
                static COLOR_TABLE: BlockTable<BlockColor> =
            }
            repeat = brackets {
                #COLOR,
            }
            postfix = { ; }
        }

        COLOR_TABLE[self]
    }

    #[inline]
    pub fn name_fr(self) -> &'static str {
        expand_all_blocks! {
            prefix = {
                static NAME_FR_TABLE: BlockTable<&str> =
            }
            repeat = brackets {
                #FR,
            }
            postfix = { ; }
        }

        &NAME_FR_TABLE[self]
    }
}

impl<T> Index<Block> for BlockTable<T> {
    type Output = T;

    #[inline]
    fn index(&self, idx: Block) -> &T {
        // SAFETY: all block ids are less than NB_BLOCKS
        unsafe { self.get_unchecked(idx as usize) }
    }
}

impl<T> IndexMut<Block> for BlockTable<T> {
    #[inline]
    fn index_mut(&mut self, idx: Block) -> &mut T {
        // SAFETY: all block ids are less than NB_BLOCKS
        unsafe { self.get_unchecked_mut(idx as usize) }
    }
}

/// Transmute a vec without reallocating.
///
/// # Safety
///
/// `std::mem::transmute::<T, U>` must be safe to call on each element.
///
/// # Panics
///
/// This will panic if `T` and `U` have differing size of alignment.
unsafe fn transmute_vec<T, U>(vec: Vec<T>) -> Vec<U> {
    use std::alloc::Layout;
    assert_eq!(Layout::new::<T>(), Layout::new::<U>());

    let mut vec = std::mem::ManuallyDrop::new(vec);
    Vec::from_raw_parts(vec.as_mut_ptr() as *mut U, vec.len(), vec.capacity())
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum BlockColor {
    Invisible,
    Fixed(Rgb<u8>),
    Shaded(Rgb<u8>),
}

impl BlockColor {
    pub fn to_rgb(self, height: usize) -> Rgb<u8> {
        const MAX_HEIGHT: usize = crate::region::CHUNK_HEIGHT - 1;

        match self {
            Self::Invisible => Rgb([128, 128, 128]),
            Self::Fixed(rgb) => rgb,
            Self::Shaded(base) => {
                let height = std::cmp::min(height, MAX_HEIGHT) / 2;

                let factor = 0.9 + 0.6 * height as f32 / 16.0;
                let shaded = |val: u8| {
                    let v = val as f32 * factor;
                    if v > 255.0 {
                        255
                    } else {
                        v as u8
                    }
                };

                Rgb([shaded(base[0]), shaded(base[1]), shaded(base[2])])
            }
        }
    }
}
