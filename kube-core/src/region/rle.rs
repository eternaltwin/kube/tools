use thiserror::Error;

#[derive(Debug, Error)]
pub enum RleDecodeError {
    #[error("RLE error: not enough data")]
    NotEnoughData,
    #[error("RLE error: output buffer too small")]
    BufferTooSmall,
}

#[derive(Debug, Eq, PartialEq)]
struct RunHeader {
    len: usize,
    repeat: Option<u8>,
}

#[allow(clippy::unusual_byte_groupings)]
impl RunHeader {
    const MASK_REPEAT: u8 = 0b1_0000000;
    const MASK_LEN_1B: u8 = 0b00_000000;
    const MASK_LEN_2B: u8 = 0b010_00000;
    const MASK_LEN_3B: u8 = 0b011_00000;
    const MAX_LENGTH1: usize = 0x3F;
    const MAX_LENGTH2: usize = 0x1FFF;
    const MAX_LENGTH3: usize = 0x1FFFFF;
    const MAX_LENGTH: usize = Self::MAX_LENGTH3 + 1;
}

impl RunHeader {
    #[allow(clippy::match_overlapping_arm)]
    fn write(&self, buf: &mut Vec<u8>) {
        let mask = if self.repeat.is_some() {
            Self::MASK_REPEAT
        } else {
            0x00
        };

        match self.len {
            0 => return, // Empty run, nothing to write
            0..=Self::MAX_LENGTH1 => {
                buf.push(mask | Self::MASK_LEN_1B | (self.len as u8));
            }
            0..=Self::MAX_LENGTH2 => {
                buf.push(mask | Self::MASK_LEN_2B | (self.len >> 8) as u8);
                buf.push((self.len & 0xFF) as u8);
            }
            0..=Self::MAX_LENGTH3 => {
                buf.push(mask | Self::MASK_LEN_3B | (self.len >> 16) as u8);
                buf.push(((self.len >> 8) & 0xFF) as u8);
                buf.push((self.len & 0xFF) as u8);
            }
            Self::MAX_LENGTH => {
                // special case, encode maximum length as 0
                buf.push(mask | Self::MASK_LEN_1B);
            }
            _ => panic!("run length too big: {}", self.len),
        };

        if let Some(byte) = self.repeat {
            buf.push(byte);
        }
    }

    fn assert_len(buf: &[u8], len: usize) -> Result<(), RleDecodeError> {
        if len > buf.len() {
            Err(RleDecodeError::NotEnoughData)
        } else {
            Ok(())
        }
    }

    fn parse(buf: &[u8]) -> Result<(Self, usize), RleDecodeError> {
        let first = *buf.get(0).ok_or(RleDecodeError::NotEnoughData)?;
        let repeat = (first & Self::MASK_REPEAT) != 0;

        let (len, pos) = if (first & Self::MASK_LEN_3B) == Self::MASK_LEN_3B {
            Self::assert_len(buf, 3)?;
            let len = (buf[0] as usize) << 16 | (buf[1] as usize) << 8 | (buf[2] as usize);
            (len & Self::MAX_LENGTH3, 3)
        } else if (first & Self::MASK_LEN_2B) == Self::MASK_LEN_2B {
            Self::assert_len(buf, 2)?;
            let len = (buf[0] as usize) << 8 | (buf[1] as usize);
            (len & Self::MAX_LENGTH2, 2)
        } else {
            ((first as usize) & Self::MAX_LENGTH1, 1)
        };
        // Special case, maximum length is encoded as 0
        let len = if len == 0 { Self::MAX_LENGTH } else { len };

        if repeat {
            Ok((
                Self {
                    len,
                    repeat: Some(*buf.get(pos).ok_or(RleDecodeError::NotEnoughData)?),
                },
                pos + 1,
            ))
        } else {
            Ok((Self { len, repeat: None }, pos))
        }
    }
}

struct RunIter<I: Iterator> {
    iter: std::iter::Peekable<I>,
}

impl<I: Iterator> RunIter<I> {
    fn new(iter: I) -> Self {
        Self {
            iter: iter.peekable(),
        }
    }
}

impl<I: Iterator> Iterator for RunIter<I>
where
    I::Item: Eq,
{
    type Item = (usize, I::Item);

    fn next(&mut self) -> Option<Self::Item> {
        let mut len = 1;
        let cur = self.iter.next()?;

        while self.iter.peek() == Some(&cur) {
            if len >= RunHeader::MAX_LENGTH {
                break;
            }
            len += 1;
            self.iter.next();
        }

        Some((len, cur))
    }
}

pub fn encode(buf: &mut Vec<u8>, data: &[u8]) {
    const MIN_RUN_LENGTH: usize = 4;

    fn write_raw(buf: &mut Vec<u8>, data: &[u8]) {
        RunHeader {
            len: data.len(),
            repeat: None,
        }
        .write(buf);
        buf.extend_from_slice(data);
    }

    let mut raw_start = None;
    let mut offset = 0;
    for (repeat, byte) in RunIter::new(data.iter().cloned()) {
        if repeat < MIN_RUN_LENGTH {
            raw_start = Some(raw_start.unwrap_or(offset));
        } else {
            if let Some(raw) = raw_start {
                write_raw(buf, &data[raw..offset]);
                raw_start = None;
            }
            RunHeader {
                len: repeat,
                repeat: Some(byte),
            }
            .write(buf);
        }
        offset += repeat;
    }

    if let Some(raw) = raw_start {
        write_raw(buf, &data[raw..offset]);
    }
}

pub fn decode(mut buf: &mut [u8], mut data: &[u8]) -> Result<usize, RleDecodeError> {
    let mut total = 0;

    while !data.is_empty() {
        let (run, read) = RunHeader::parse(data)?;
        data = &data[read..];

        RunHeader::assert_len(buf, run.len).or(Err(RleDecodeError::BufferTooSmall))?;
        let cur = buf.split_at_mut(run.len);
        buf = cur.1;
        let cur = cur.0;
        total += run.len;

        if let Some(byte) = run.repeat {
            for b in cur {
                *b = byte;
            }
        } else {
            RunHeader::assert_len(data, run.len)?;
            let split = data.split_at(run.len);
            cur.copy_from_slice(split.0);
            data = split.1;
        }
    }

    Ok(total)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn runs_iter() {
        let data = b"---*--***-*";
        let runs: &[(usize, u8)] = &[
            (3, b'-'),
            (1, b'*'),
            (2, b'-'),
            (3, b'*'),
            (1, b'-'),
            (1, b'*'),
        ];

        let result: Vec<_> = RunIter::new(data.iter().cloned()).collect();
        assert_eq!(runs, &result[..]);
    }

    #[test]
    fn run_headers() {
        let headers: &[RunHeader] = &[
            RunHeader {
                len: 4,
                repeat: None,
            },
            RunHeader {
                len: 63,
                repeat: Some(42),
            },
            RunHeader {
                len: 200,
                repeat: Some(10),
            },
            RunHeader {
                len: 65536,
                repeat: Some(0),
            },
        ];

        let mut out = Vec::new();
        for header in headers {
            out.clear();
            header.write(&mut out);
            let (parsed, _) = RunHeader::parse(&mut out).unwrap();
            assert_eq!(header, &parsed);
        }
    }
}
