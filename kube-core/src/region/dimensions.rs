use crate::region::storage::{Dimensions, PlaneIdx, PrivateDimensions, RegionIdx};

/// Dimensions for a chunk-sized region (256x256x32).
#[derive(Debug, Default, Copy, Clone, Eq, PartialEq)]
pub struct ChunkDim;

unsafe impl PrivateDimensions for ChunkDim {
    type Iter3D = ChunkDimIter3D;
    type Iter2D = ChunkDimIter2D;

    #[inline(always)]
    fn _width(self) -> usize {
        super::CHUNK_WIDTH
    }
    #[inline(always)]
    fn _depth(self) -> usize {
        super::CHUNK_WIDTH
    }
    #[inline(always)]
    fn _height(self) -> usize {
        super::CHUNK_HEIGHT
    }
    #[inline(always)]
    fn _iter_3d(self) -> Self::Iter3D {
        ChunkDimIter3D(0)
    }
    #[inline(always)]
    fn _iter_2d(self) -> Self::Iter2D {
        ChunkDimIter2D(0)
    }

    #[inline]
    fn _raw_idx_3d(self, (x, y, z): RegionIdx) -> Option<usize> {
        if x >= self.width() || y >= self.depth() || z >= self.height() {
            None
        } else {
            Some(x | (y << 8) | (z << 16))
        }
    }

    #[inline]
    fn _raw_idx_2d(self, (x, y): PlaneIdx) -> Option<usize> {
        if x >= self.width() || y >= self.depth() {
            None
        } else {
            Some(x | (y << 8))
        }
    }

    #[inline(always)]
    fn _add_z_component(self, raw_idx: usize, height: usize) -> usize {
        raw_idx | (height << 16)
    }
}

pub struct ChunkDimIter3D(usize);

impl Iterator for ChunkDimIter3D {
    type Item = (RegionIdx, usize);

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        let idx = self.0;
        if idx >= ChunkDim.volume() {
            return None;
        }

        self.0 += 1;
        let x = idx & 0xFF;
        let y = (idx >> 8) & 0xFF;
        let z = idx >> 16;
        Some(((x, y, z), idx))
    }
}

pub struct ChunkDimIter2D(usize);

impl Iterator for ChunkDimIter2D {
    type Item = (PlaneIdx, usize);

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        let idx = self.0;
        if idx >= ChunkDim.area() {
            return None;
        }

        self.0 += 1;
        let x = idx & 0xFF;
        let y = idx >> 8;
        Some(((x, y), idx))
    }
}

/// Dimensions for region with arbitrary width and depth, but fixed height (32).
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct DynDim {
    width: usize,
    depth: usize,
}

impl DynDim {
    // TODO: const fn when this is stabilised
    /// Panics if the size would overflow.
    pub fn new(width: usize, depth: usize) -> Self {
        width
            .checked_mul(depth)
            .and_then(|s| s.checked_mul(super::CHUNK_HEIGHT))
            .expect("region dimensions overflow");
        Self { width, depth }
    }
}

unsafe impl PrivateDimensions for DynDim {
    type Iter3D = DynDimIter3D;
    type Iter2D = DynDimIter2D;

    #[inline(always)]
    fn _width(self) -> usize {
        self.width
    }
    #[inline(always)]
    fn _depth(self) -> usize {
        self.depth
    }
    #[inline(always)]
    fn _height(self) -> usize {
        super::CHUNK_HEIGHT
    }
    #[inline(always)]
    fn _iter_3d(self) -> Self::Iter3D {
        DynDimIter3D {
            dims: self,
            x: 0,
            y: 0,
            z: 0,
            raw: 0,
            max: self.volume(),
        }
    }
    #[inline(always)]
    fn _iter_2d(self) -> Self::Iter2D {
        DynDimIter2D {
            width: self.width(),
            x: 0,
            y: 0,
            raw: 0,
            max: self.area(),
        }
    }

    #[inline]
    fn _raw_idx_3d(self, (x, y, z): RegionIdx) -> Option<usize> {
        if x >= self.width() || y >= self.depth() || z >= self.height() {
            None
        } else {
            Some(x + (y * self.width()) + (z * self.width() * self.depth()))
        }
    }

    #[inline]
    fn _raw_idx_2d(self, (x, y): PlaneIdx) -> Option<usize> {
        if x >= self.width() || y >= self.depth() {
            None
        } else {
            Some(x + (y * self.width()))
        }
    }

    #[inline(always)]
    fn _add_z_component(self, raw_idx: usize, height: usize) -> usize {
        raw_idx + height * self.width() * self.depth()
    }
}

pub struct DynDimIter3D {
    dims: DynDim,
    x: usize,
    y: usize,
    z: usize,
    raw: usize,
    max: usize,
}

impl Iterator for DynDimIter3D {
    type Item = (RegionIdx, usize);

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        if self.raw >= self.max {
            return None;
        }

        let r = ((self.x, self.y, self.z), self.raw);
        self.raw += 1;
        self.x += 1;
        if self.x >= self.dims.width() {
            self.x = 0;
            self.y += 1;
        }
        if self.y >= self.dims.depth() {
            self.y = 0;
            self.z += 1;
        }
        Some(r)
    }
}

pub struct DynDimIter2D {
    width: usize,
    x: usize,
    y: usize,
    raw: usize,
    max: usize,
}

impl Iterator for DynDimIter2D {
    type Item = (PlaneIdx, usize);

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        if self.raw >= self.max {
            return None;
        }

        let r = ((self.x, self.y), self.raw);
        self.raw += 1;
        self.x += 1;
        if self.x >= self.width {
            self.x = 0;
            self.y += 1;
        }
        Some(r)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn test_correctness<D: Dimensions>(dim: D) {
        let mut iter = dim._iter_3d();
        for raw in 0..dim.volume() {
            let idx = {
                let (x, rest) = (raw % dim.width(), raw / dim.width());
                let (y, z) = (rest % dim.depth(), rest / dim.depth());
                (x, y, z)
            };

            assert_eq!(Some((idx, raw)), iter.next(), "wrong iter3d");
            assert_eq!(
                Some(raw),
                dim._raw_idx_3d(idx),
                "wrong raw_idx_3d for {:?}",
                idx
            );
        }
        assert_eq!(None, iter.next());

        let mut iter = dim._iter_2d();
        for raw in 0..dim.area() {
            let idx = (raw % dim.width(), raw / dim.width());

            assert_eq!(Some((idx, raw)), iter.next(), "wrong iter2d");
            assert_eq!(
                Some(raw),
                dim._raw_idx_2d(idx),
                "wrong raw_idx_2d for {:?}",
                idx
            );
        }
        assert_eq!(None, iter.next());
    }

    #[test]
    fn chunk_dims() {
        test_correctness(ChunkDim);
    }

    #[test]
    fn dyn_dims() {
        test_correctness(DynDim::new(100, 0));
        test_correctness(DynDim::new(100, 100));
    }

    #[test]
    #[should_panic(expected = "region dimensions overflow")]
    fn dyn_dims_overflow_panic() {
        DynDim::new(0x1_0000_0000, 0x1_0000_0000);
    }
}
