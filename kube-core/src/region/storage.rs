use std::ops::{Bound, Range, RangeBounds};

use super::{BlockPos, PlanePos, RegionError};

pub type RegionIdx = (usize, usize, usize);
pub type PlaneIdx = (usize, usize);

pub trait Dimensions: PrivateDimensions {
    // TODO: make const fns when usable on stable
    fn width(self) -> usize;
    fn depth(self) -> usize;
    fn height(self) -> usize;

    fn dims(self) -> RegionIdx;
    fn area(self) -> usize;
    fn volume(self) -> usize;
}

/// # Safety
///
/// Implementors of this trait MUST garantee that the various methods return
/// correct indices (in particular, it must never overflow).
pub unsafe trait PrivateDimensions: Sized + Copy {
    type Iter3D: Iterator<Item = (RegionIdx, usize)>;
    type Iter2D: Iterator<Item = (PlaneIdx, usize)>;

    fn _width(self) -> usize;
    fn _depth(self) -> usize;
    fn _height(self) -> usize;

    // Returns an iterator yielding indices for each element in the region,
    // in z, y, x order, together with the corresponding raw indices.
    fn _iter_3d(self) -> Self::Iter3D;

    // Returns an iterator yielding indices for each element in the plane,
    // in y, x order, together with the corresponding raw indices.
    fn _iter_2d(self) -> Self::Iter2D;

    // Returns the raw index for the given coordinates,
    // if the coordinates are in bounds.
    fn _raw_idx_3d(self, idx: RegionIdx) -> Option<usize>;

    // Returns the raw index for the given 2D coordinates,
    // if the coordinates are in bounds.
    fn _raw_idx_2d(self, idx: PlaneIdx) -> Option<usize>;

    // Add the given height component to a raw 2D index, and return
    // the corresponding 3D index.
    fn _add_z_component(self, raw_idx: usize, height: usize) -> usize;
}

impl<D> Dimensions for D
where
    D: PrivateDimensions + Sized,
{
    #[inline(always)]
    fn width(self) -> usize {
        self._width()
    }

    #[inline(always)]
    fn depth(self) -> usize {
        self._depth()
    }

    #[inline(always)]
    fn height(self) -> usize {
        self._height()
    }

    #[inline(always)]
    fn dims(self) -> (usize, usize, usize) {
        (self._width(), self._depth(), self._height())
    }

    #[inline(always)]
    fn area(self) -> usize {
        self._width() * self._depth()
    }

    #[inline(always)]
    fn volume(self) -> usize {
        self._width() * self._depth() * self._height()
    }
}

/// Full 3D region data.
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct RegionStorage<T, D> {
    pub(super) data: Box<[T]>,
    pub(super) dims: D,
}

/// A 2D horizontal slice.
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct PlaneStorage<T, D> {
    pub(super) data: Box<[T]>,
    pub(super) dims: D,
}

macro_rules! impl_common_storage {(
    meta {
        type ($ty_storage:ident, $ty_idx:ident, $ty_pos:ident);
        fn (
            size: $fn_size:ident,
            iter: $fn_iter:ident,
            global_iter: $fn_global_iter:ident,
            idx: $fn_raw_idx:ident,
            check: $fn_check_dims:ident,
        );
    }
    $($extra:tt)*
) => {
impl<T, D: Dimensions> $ty_storage<T, D> {

    #[inline]
    pub fn new() -> Self where D: Default, T: Default {
        Self::new_sized(D::default())
    }

    pub fn new_sized(dims: D) -> Self where T: Default {
        let size = dims.$fn_size();
        let mut data = Vec::with_capacity(size);
        // TODO: not that efficient for Copy types?
        data.resize_with(size, Default::default);
        let data = data.into_boxed_slice();
        Self { data, dims }
    }

    #[inline]
    pub fn new_with(elem: T) -> Self where D: Default, T: Clone {
        Self::new_with_sized(D::default(), elem)
    }

    pub fn new_with_sized(dims: D, elem: T) -> Self where T: Clone {
        let size = dims.$fn_size();
        let mut data = Vec::with_capacity(size);
        data.resize(size, elem);
        let data = data.into_boxed_slice();
        Self { data, dims }
    }

    #[inline]
    pub fn from_vec(data: Vec<T>) -> Result<Self, RegionError> where D: Default {
        Self::from_vec_sized(D::default(), data)
    }

    pub fn from_vec_sized(dims: D, data: Vec<T>) -> Result<Self, RegionError> {
        let size = dims.$fn_size();
        if data.len() == size {
            let data = data.into_boxed_slice();
            Ok(Self { data, dims })
        } else {
            Err(RegionError::InvalidSize {
                expected: size,
                actual: Some(data.len()),
            })
        }
    }

    #[inline]
    pub fn from_fn(f: impl FnMut($ty_idx) -> T) -> Self where D: Default {
        Self::from_fn_sized(D::default(), f)
    }

    pub fn from_fn_sized(dims: D, mut f: impl FnMut($ty_idx) -> T) -> Self {
        let data = dims.$fn_iter().map(|(idx, _)| f(idx)).collect();
        Self { data, dims }
    }

    #[inline]
    pub fn dims(&self) -> D { self.dims }

    #[inline]
    pub fn size(&self) -> usize { self.dims.$fn_size() }

    #[inline]
    pub fn get(&self, idx: $ty_idx) -> Option<&T> {
        // Safe, raw_idx does the bounds check.
        self.dims.$fn_raw_idx(idx).map(|i| unsafe {
            self.data.get_unchecked(i)
        })
    }

    #[inline]
    pub fn get_mut(&mut self, idx: $ty_idx) -> Option<&mut T> {
        // Safe, raw_idx does the bounds check.
        self.dims.$fn_raw_idx(idx).map(move |i| unsafe {
            self.data.get_unchecked_mut(i)
        })
    }

    pub fn copy_from_slice(&mut self, data: &[T]) -> Result<(), RegionError> where T: Copy {
        let size = self.dims.$fn_size();
        if data.len() == size {
            self.data.copy_from_slice(data);
            Ok(())
        } else {
            Err(RegionError::InvalidSize {
                expected: size,
                actual: Some(data.len()),
            })
        }
    }

    pub fn copy_from(&mut self, other: &Self) where T: Copy {
        $fn_check_dims(&self.dims, &other.dims, "copy");
        self.data.copy_from_slice(&other.data)
    }

    #[inline]
    pub fn as_slice(&self) -> &[T] {
        &self.data
    }

    #[inline]
    pub fn into_vec(self) -> Vec<T> {
        self.data.into_vec()
    }

    #[inline]
    pub fn blocks<'a>(&'a self) -> impl Iterator<Item=($ty_idx, &'a T)> {
        self.dims.$fn_iter().map(move |(idx, raw)| {
            // Safe, all indices returned by `dim.iter()` are valid.
            (idx, unsafe { self.data.get_unchecked(raw) })
        })
    }

    #[inline]
    pub fn blocks_mut<'a>(&'a mut self) -> impl Iterator<Item=($ty_idx, &'a mut T)> {
        let dims = self.dims;
        // TODO: is this performant enough?
        self.data.iter_mut()
            .zip(dims.$fn_iter())
            .map(|(block, (idx, _))| (idx, block))
    }

    #[inline]
    pub fn blocks_global(&self, offset: $ty_pos) -> impl Iterator<Item=($ty_pos, &T)> {
        $fn_global_iter(offset, self.blocks())
    }

    #[inline]
    pub fn blocks_global_mut(&mut self, offset: $ty_pos) -> impl Iterator<Item=($ty_pos, &mut T)> {
        $fn_global_iter(offset, self.blocks_mut())
    }

    $($extra)*
}

impl<T, D: Dimensions> std::ops::Index<$ty_idx> for $ty_storage<T, D> {
    type Output = T;

    #[inline(always)]
    fn index(&self, idx: $ty_idx) -> &T {
        if let Some(idx) = self.dims.$fn_raw_idx(idx) {
            // Safe, raw_idx does the bounds check.
            unsafe { self.data.get_unchecked(idx) }
        } else {
            panic!("Region block index out of bounds: {:?}", idx);
        }
    }
}

impl<T, D: Dimensions> std::ops::IndexMut<$ty_idx> for $ty_storage<T, D> {
    #[inline(always)]
    fn index_mut(&mut self, idx: $ty_idx) -> &mut T {
        if let Some(idx) = self.dims.$fn_raw_idx(idx) {
            // Safe, raw_idx does the bounds check.
            unsafe { self.data.get_unchecked_mut(idx) }
        } else {
            panic!("Region block index out of bounds: {:?}", idx);
        }
    }
}

impl<D: Dimensions + Default, T: Default> Default for $ty_storage<T, D> {
    #[inline]
    fn default() -> Self {
        Self::new()
    }
}

}}

impl_common_storage! {
    meta {
        type (PlaneStorage, PlaneIdx, PlanePos);
        fn(
            size: area,
            iter: _iter_2d,
            global_iter: indices_to_global_2d,
            idx: _raw_idx_2d,
            check: check_2d_dims,
        );
    }
}

impl_common_storage! {
    meta {
        type (RegionStorage, RegionIdx, BlockPos);
        fn(
            size: volume,
            iter: _iter_3d,
            global_iter: indices_to_global_3d,
            idx: _raw_idx_3d,
            check: check_3d_dims,
        );
    }

    pub fn copy_region<D2: Dimensions, Rx: RangeBounds<usize>, Ry: RangeBounds<usize>, Rz: RangeBounds<usize>>(
            &mut self, other: &RegionStorage<T, D2>, dst: RegionIdx,
            (src_x, src_y, src_z): (Rx, Ry, Rz)) where T: Copy {
        let src_x = resolve_bound(src_x, self.dims.width(), dst.0, other.dims.width());
        let src_y = resolve_bound(src_y, self.dims.depth(), dst.1, other.dims.depth());
        let src_z = resolve_bound(src_z, self.dims.height(), dst.2, other.dims.height());
        let (src_x, src_y, src_z) = match (src_x, src_y, src_z) {
            (Some(x), Some(y), Some(z)) => (x, y, z),
            _ => panic!("can't copy region, out-of-bounds"), // TODO: better error message?
        };

        // The offsets may be negative, so wrap-around in this case.
        let off_x = dst.0.wrapping_sub(src_x.start);
        let off_y = dst.1.wrapping_sub(src_y.start);
        let off_z = dst.2.wrapping_sub(src_z.start);

        for z in src_z {
            for y in src_y.clone() {
                for x in src_x.clone() {
                    // TODO: unsafe for performance?
                    let dst_idx = (x.wrapping_add(off_x), y.wrapping_add(off_y), z.wrapping_add(off_z));
                    let dst_raw = self.dims._raw_idx_3d(dst_idx).unwrap();
                    let src_raw = other.dims._raw_idx_3d((x, y, z)).unwrap();
                    self.data[dst_raw] = other.data[src_raw];
                }
            }
        }
    }

    pub fn flatten<U: Default>(&self, flatten: impl FnMut(&mut U, RegionIdx, &T)) -> PlaneStorage<U, D> {
        self.flatten_with(|_| Default::default(), flatten)
    }

    pub fn flatten_with<U>(&self, init: impl FnMut(PlaneIdx) -> U, flatten: impl FnMut(&mut U, RegionIdx, &T)) -> PlaneStorage<U, D> {
        let mut plane = PlaneStorage::from_fn_sized(self.dims(), init);
        self.flatten_in(&mut plane, flatten);
        plane
    }

    pub fn flatten_in<U>(&self, plane: &mut PlaneStorage<U, D>, mut flatten: impl FnMut(&mut U, RegionIdx, &T)) {
        check_2d_dims(&plane.dims, &self.dims, "fold region");

        for z in 0..self.dims.height() {
            self.dims._iter_2d().for_each(|((x, y), raw2d)| {
                let raw3d = self.dims._add_z_component(raw2d, z);

                // Safe, because self.dims and region.dims are compatible.
                let (block_2d, block_3d) = unsafe {(
                    plane.data.get_unchecked_mut(raw2d),
                    self.data.get_unchecked(raw3d),
                )};

                flatten(block_2d, (x, y, z), block_3d);
            });
        }
    }
}

fn indices_to_global_3d<T>(
    offset: BlockPos,
    iter: impl Iterator<Item = (RegionIdx, T)>,
) -> impl Iterator<Item = (BlockPos, T)> {
    // TODO: check overflow?
    iter.map(move |(idx, v)| {
        let pos = (
            offset.0 + idx.0 as i64,
            offset.1 + idx.1 as i64,
            offset.2 + idx.2 as i64,
        );
        (pos, v)
    })
}

fn indices_to_global_2d<T>(
    offset: PlanePos,
    iter: impl Iterator<Item = (PlaneIdx, T)>,
) -> impl Iterator<Item = (PlanePos, T)> {
    // TODO: check overflow?
    iter.map(move |(idx, v)| {
        let pos = (offset.0 + idx.0 as i64, offset.1 + idx.1 as i64);
        (pos, v)
    })
}

fn check_3d_dims<D1: Dimensions, D2: Dimensions>(dim: &D1, other: &D2, action: &str) {
    if dim.dims() != other.dims() {
        panic!(
            "can't {}, dims are different: {:?} != {:?}",
            action,
            dim.dims(),
            other.dims()
        );
    }
}

fn check_2d_dims<D1: Dimensions, D2: Dimensions>(dim: &D1, other: &D2, action: &str) {
    let dim = (dim.width(), other.depth());
    let other = (other.width(), other.depth());
    if dim != other {
        panic!(
            "can't {}, dims are different: {:?} != {:?}",
            action, dim, other
        );
    }
}

fn resolve_bound<B: RangeBounds<usize>>(
    bound: B,
    dst_size: usize,
    dst_offset: usize,
    src_size: usize,
) -> Option<Range<usize>> {
    let start = match bound.start_bound() {
        Bound::Included(n) => *n,
        Bound::Excluded(n) => n.checked_add(1)?,
        Bound::Unbounded => 0,
    };
    let end = match bound.end_bound() {
        Bound::Included(n) => n.checked_add(1)?,
        Bound::Excluded(n) => *n,
        Bound::Unbounded => src_size,
    };
    if start > end || end > src_size || (end - start).saturating_add(dst_offset) > dst_size {
        None
    } else {
        Some(start..end)
    }
}
