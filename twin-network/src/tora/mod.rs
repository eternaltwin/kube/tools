use std::io;
use std::sync::Arc;

use thiserror::Error;

mod pool;
mod protocol;
pub mod raw;

pub use pool::{ToraPooledSender, ToraReceiverPool, ToraSenderPool};
pub use protocol::{MessageBuilder, ToraReceiver, ToraSender};
use raw::{Code, FrameBuilder};

#[derive(Debug, Error)]
pub enum ToraError {
    #[error("unsupported Tora code: {0:?}")]
    UnsupportedCode(Code),
    #[error("remote Tora error: {0}")]
    RemoteError(String),
    #[error("couldn't establish Tora connection: {0}")]
    ConnectionError(#[source] io::Error),
    #[error(transparent)]
    IoError(#[from] io::Error),
    #[error("error while building Tora message: {0}")]
    BuilderError(#[source] BoxError),
    // TODO: remove this variant?
    #[error("invalid Tora data: {0}")]
    InvalidData(#[source] BoxError),
}

pub type Result<T> = std::result::Result<T, ToraError>;
type BoxError = Box<dyn std::error::Error + Sync + Send>;
type BoxResult<T> = std::result::Result<T, BoxError>;

#[derive(Debug, Clone)]
pub struct ToraAddr {
    host: Arc<str>,
    port: u16,
}

impl ToraAddr {
    pub fn new(host: &str, port: u16) -> Self {
        let host = Arc::from(host);
        Self { host, port }
    }

    pub async fn connect(&self) -> Result<(ToraReceiver, ToraSender)> {
        protocol::connect(self).await
    }

    pub async fn connect_pool(
        &self,
        max_connections: usize,
    ) -> Result<(ToraReceiverPool, ToraSenderPool)> {
        pool::connect_pool(self, max_connections).await
    }
}
