use std::io;
use std::ops::Range;

use thiserror::Error;
use tokio::io::{AsyncRead, AsyncReadExt};

macro_rules! declare_code_enum {
    ($(#[$meta:meta])* $vis:vis enum $name:ident {
        $($variant:ident = $id:expr),* $(,)?
    }) => {
        $(#[$meta])*
        $vis enum $name {
           $($variant),*
        }

        impl $name {
            fn from_byte(b: u8) -> Option<Self> {
                match b {
                    $($id => Some(Self::$variant),)*
                    _ => None,
                }
            }

            fn to_byte(self) -> u8 {
                match self {
                    $(Self::$variant => $id),*
                }
            }
        }
    }
}

declare_code_enum!(
    /// See: <https://github.com/HaxeFoundation/tora/blob/master/tora/Code.hx>
    #[derive(Copy, Clone, Debug, Eq, PartialEq)]
    pub enum Code {
        File = 1,
        Uri = 2,
        ClientIP = 3,
        GetParams = 4,
        PostData = 5,
        HeaderKey = 6,
        HeaderValue = 7,
        HeaderAddValue = 8,
        ParamKey = 9,
        ParamValue = 10,
        HostName = 11,
        HttpMethod = 12,
        Execute = 13,
        Error = 14,
        Print = 15,
        Log = 16,
        Flush = 17,
        Redirect = 18,
        ReturnCode = 19,
        QueryMultipart = 20,
        PartFilename = 21,
        PartKey = 22,
        ParData = 23,
        PartDone = 24,
        TestConnect = 25,
        Listen = 26,
        HostResolve = 27,
    }
);

pub const MAX_MSG_LEN: usize = 0xFFFFFF;
const HEADER_LEN: usize = 4;

impl Code {
    fn parse_header(header: [u8; HEADER_LEN]) -> Option<(Code, usize)> {
        let code = Code::from_byte(header[0])?;
        let len = (header[1] as usize) | ((header[2] as usize) << 8) | ((header[3] as usize) << 16);
        Some((code, len))
    }

    fn emit_header(self, msg_len: usize) -> [u8; HEADER_LEN] {
        [
            self.to_byte(),
            (msg_len & 0xFF) as u8,
            ((msg_len >> 8) & 0xFF) as u8,
            ((msg_len >> 16) & 0xFF) as u8,
        ]
    }
}

#[derive(Debug, Error)]
#[error("maximum Tora frame length exceeded")]
pub struct FrameSizeExceeded(usize);

impl FrameSizeExceeded {
    pub fn bytes_written(&self) -> usize {
        self.0
    }

    fn into_io_error(self) -> io::Error {
        io::Error::new(
            io::ErrorKind::WriteZero,
            "maximum Tora frame length exceeded",
        )
    }
}

#[must_use = "this builder won't do anything unless a a terminal operation is called"]
pub struct FrameBuilder<'a> {
    buf: &'a mut Vec<u8>,
    code: Code,
    start: usize,
    discard_pos: usize,
}

impl<'a> FrameBuilder<'a> {
    pub fn begin(buf: &'a mut Vec<u8>, code: Code) -> Self {
        Self::begin_with_discard(buf, code, buf.len())
    }

    pub(super) fn begin_with_discard(buf: &'a mut Vec<u8>, code: Code, discard_pos: usize) -> Self {
        // Add dummy header; if Self is forgotten, this will be an invalid message
        buf.extend_from_slice(&[0; HEADER_LEN]);
        let start = buf.len();
        Self {
            buf,
            code,
            start,
            discard_pos,
        }
    }

    pub(super) fn content_range(&self) -> Range<usize> {
        self.start..self.buf.len()
    }

    pub(super) fn buf(&self) -> &[u8] {
        self.buf
    }

    pub fn contents(&self) -> &[u8] {
        &self.buf[self.start..]
    }

    pub fn contents_mut(&mut self) -> &mut [u8] {
        &mut self.buf[self.start..]
    }

    pub fn push(&mut self, bytes: &[u8]) -> Result<(), FrameSizeExceeded> {
        let available = MAX_MSG_LEN - self.contents().len();
        let (res, bytes) = if bytes.len() > available {
            (Err(FrameSizeExceeded(available)), &bytes[..available])
        } else {
            (Ok(()), bytes)
        };
        self.buf.extend_from_slice(bytes);
        res
    }

    pub fn finish(self) {
        let header_start = self.start - HEADER_LEN;
        let msg_len = self.buf.len() - self.start;
        let header = self.code.emit_header(msg_len);
        self.buf[header_start..self.start].copy_from_slice(&header);
        std::mem::forget(self); // Don't discard buffer contents
    }

    pub fn discard(self) {
        drop(self)
    }
}

impl<'a> Drop for FrameBuilder<'a> {
    fn drop(&mut self) {
        self.buf.truncate(self.discard_pos);
    }
}

impl<'a> std::io::Write for FrameBuilder<'a> {
    #[inline]
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        match self.push(buf) {
            Ok(()) => Ok(buf.len()),
            Err(err) => Ok(err.bytes_written()),
        }
    }

    #[inline]
    fn write_all(&mut self, buf: &[u8]) -> io::Result<()> {
        self.push(buf).map_err(FrameSizeExceeded::into_io_error)
    }

    #[inline]
    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }
}

pub async fn read_frame<R: AsyncRead + Unpin>(
    reader: &mut R,
    buf: &mut Vec<u8>,
    start: usize,
) -> io::Result<Option<(Code, usize)>> {
    let mut header = [0; HEADER_LEN];
    let len = reader.read(&mut header).await?;
    if len == 0 {
        return Ok(None); // Reached end of stream
    } else if len < HEADER_LEN {
        // read the rest of the header
        reader.read_exact(&mut header[len..]).await?;
    }

    let (code, len) = Code::parse_header(header)
        .ok_or_else(|| io::Error::new(io::ErrorKind::InvalidData, "invalid Tora code"))?;

    let end = start.checked_add(len).expect("buffer overflow");
    if buf.len() < end {
        buf.resize(end, 0);
    }

    reader.read_exact(&mut buf[start..end]).await?;
    Ok(Some((code, len)))
}
