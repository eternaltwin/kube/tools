use std::path::PathBuf;
use std::str::FromStr;
use std::time::Duration;

use chrono::{DateTime, Utc, TimeZone as _};
use clap::Parser;
use kube_core::minimap::AxisOrientation;
use kube_core::region::{ChunkPos, CHUNK_HEIGHT, ZONES_PER_CHUNK, ZONE_WIDTH};
use serde::Deserialize;
use twin_network::session::TwinSessionId;

use crate::region::ZoneBounds;
use crate::utils::BoxResult;

#[derive(Debug, Parser)]
pub struct CliOptions {
    #[clap(long = "config", default_value = "./Config.toml")]
    pub config_file: PathBuf,
    #[clap(subcommand)]
    pub command: CliCommand,
}

#[derive(Debug, Parser)]
pub enum CliCommand {
    #[clap(name = "upgrade-db")]
    UpgradeDb(CliUpgradeDb),

    #[clap(name = "crawl")]
    Crawl(CliCrawl),

    #[clap(name = "overview")]
    Overview(CliOverview),
    #[clap(name = "minimap")]
    Minimap(CliMinimap),
    #[clap(name = "export-raw")]
    ExportRaw(CliExportRaw),
    #[clap(name = "export-kub3dit")]
    ExportKub3dit(CliExportKub3dit),

    #[clap(name = "list-forums")]
    ListForums(CliListForums),
    #[clap(name = "enable-forum")]
    EnableForum(CliEnableForum),
    #[clap(name = "teleport")]
    Teleport(CliTeleport),
}

#[derive(Debug, Parser)]
pub struct CliUpgradeDb {
    #[clap(long = "dry-run")]
    pub dry_run: bool,
    #[clap(long = "compress-chunks")]
    pub compress_chunks: bool,
    #[clap(skip = 8usize)]
    pub parallel_jobs: usize,
}

#[derive(Debug, Parser)]
pub struct CliCrawl {
    #[clap(skip = Duration::from_millis(100))]
    pub throttle_requests: Duration,
    #[clap(skip = 10u32)]
    pub max_inflight_requests: u32,
    #[clap(skip = 50u32)]
    pub max_inflight_processes: u32,

    #[clap(flatten)]
    pub chunks: UpdateChunksSettings,
    #[clap(flatten)]
    pub tora: ToraSettings,
    #[clap(flatten)]
    pub html: KubeHtmlSettings,
}

#[derive(Debug, Parser)]
pub struct UpdateChunksSettings {
    #[clap(long = "update", short = 'u', value_parser(parse_date_from_duration))]
    pub update_chunks_older_than: Option<DateTime<Utc>>,
    #[clap(long = "start", short = 's', allow_hyphen_values = true, value_parser(parse_chunk_coordinates))]
    pub start_chunk: Option<ChunkPos>,
}

#[derive(Debug, Parser)]
pub struct KubeHtmlSettings {
    #[clap(skip = 5u32)]
    pub max_connections: u32,
    #[clap(skip = 5u32)]
    pub retry_attempts: u32,
}

#[derive(Debug, Parser)]
pub struct ToraSettings {
    #[clap(skip = 5u32)]
    pub max_connections: u32,
}

#[derive(Debug, Parser)]
pub struct CliOverview {
    #[clap()]
    pub output: PathBuf,
    #[clap(long = "zoom", short = 'z')]
    pub zoom: bool,
}

#[derive(Debug, Parser)]
pub struct CliMinimap {
    #[clap()]
    pub output: PathBuf,
    #[clap(long = "bounds", short = 'b', allow_hyphen_values = true, value_parser(parse_zone_bounds))]
    pub bounds: ZoneBounds,
    #[clap(long = "date", short = 'd', value_parser(parse_date_from_utc))]
    pub date: Option<DateTime<Utc>>,
    #[clap(long = "invert", default_value = "none", value_parser(parse_inverted_axes))]
    pub orientation: AxisOrientation,
}

#[derive(Debug, Parser)]
pub struct CliExportRaw {
    #[clap()]
    pub output: PathBuf,
    #[clap(long = "chunk", short = 'c', allow_hyphen_values = true, value_parser(parse_chunk_coordinates))]
    pub chunk: ChunkPos,
    #[clap(long = "date", short = 'd', value_parser(parse_date_from_utc))]
    pub date: Option<DateTime<Utc>>,
}

#[derive(Debug, Parser)]
pub struct CliExportKub3dit {
    #[clap()]
    pub output: PathBuf,
    #[clap(long = "bounds", short = 'b', allow_hyphen_values = true, value_parser(parse_zone_bounds))]
    pub bounds: ZoneBounds,
    #[clap(long = "date", short = 'd', value_parser(parse_date_from_utc))]
    pub date: Option<DateTime<Utc>>,
    #[clap(long = "no-minimap")]
    pub kind_no_minimap: bool,
    #[clap(long = "raw")]
    pub kind_raw: bool,
}

#[derive(Debug, Parser)]
pub struct CliListForums {
    #[clap()]
    pub output: PathBuf,
    #[clap(long = "date", short = 'd', value_parser(parse_date_from_utc))]
    pub date: Option<DateTime<Utc>>,
    #[clap(long = "invert", default_value = "none", value_parser(parse_inverted_axes))]
    pub orientation: AxisOrientation,
    #[clap(long = "format", short = 'f', default_value = "png", value_parser(parse_format_from_name))]
    pub format: image::ImageFormat,
    #[clap(long = "jobs", short = 'j', default_value = "1")]
    pub parallel_jobs: u8,
}

#[derive(Debug, Parser)]
pub struct CliEnableForum {
    #[clap(long="zone", short = 'z', allow_hyphen_values = true, value_parser(parse_zone_coordinates))]
    pub zone: (i32, i32),
}

#[derive(Debug, Parser)]
pub struct CliTeleport {
    #[clap(long = "zone", short = 'z', allow_hyphen_values = true, value_parser(parse_zone_coordinates))]
    pub zone: (i32, i32),
    #[clap(long = "pos", short = 'p', value_parser(parse_zone_position), default_value = "16:16:31")]
    pub pos: (i8, i8, i8),
}

#[derive(Debug, Deserialize)]
pub struct Config {
    pub twin_session_id: TwinSessionId,
    pub database: DatabaseConfig,
}

#[derive(Debug, Deserialize)]
pub struct DatabaseConfig {
    pub user: String,
    pub password: String,
    pub host: String,
    pub port: u16,
    pub name: String,
}

impl From<DatabaseConfig> for kube_db::Config {
    fn from(config: DatabaseConfig) -> Self {
        Self {
            user: config.user,
            password: config.password,
            host: config.host,
            port: config.port,
            name: config.name,
        }
    }
}

fn parse_zone_bounds(string: &str) -> BoxResult<ZoneBounds> {
    let mut parts = string.split(':');
    let (x, y, w, h, zoom) = match (
        parts.next(),
        parts.next(),
        parts.next(),
        parts.next(),
        parts.next(),
        parts.next(),
    ) {
        (Some(x), Some(y), Some(w), Some(h), None, None) => (x, y, w, h, None),
        (zoom, Some(x), Some(y), Some(w), Some(h), None) => (x, y, w, h, zoom),
        _ => return Err("must be x:y:w:h".into()),
    };

    let zoom = match zoom {
        None | Some("z" | "Z") => 1,
        Some("c" | "C") => ZONES_PER_CHUNK as u32,
        Some(_) => return Err("prefix must be one of c/C/z/Z".into()),
    };

    ZoneBounds::from_rect(x.parse()?, y.parse()?, w.parse()?, h.parse()?)
        .and_then(|z| z.zoom(zoom))
        .ok_or_else(|| "the bounds dimensions overflowed".into())
}

fn parse_date_from_duration(string: &str) -> BoxResult<DateTime<Utc>> {
    let duration = humantime::parse_duration(string)?;
    Ok(Utc::now() - chrono::Duration::from_std(duration)?)
}

fn parse_date_from_utc(string: &str) -> BoxResult<DateTime<Utc>> {
    let date = chrono::NaiveDateTime::parse_from_str(string, "%Y-%m-%d %H:%M:%S")?;
    Ok(Utc.from_utc_datetime(&date))
}

fn parse_chunk_coordinates(string: &str) -> BoxResult<ChunkPos> {
    let [x, y] = parse_coord_list(string).ok_or("must be cx:cy")?;
    Ok((x, y))
}

fn parse_zone_coordinates(string: &str) -> BoxResult<(i32, i32)> {
    let [x, y] = parse_coord_list(string).ok_or("must be zx:zy")?;
    Ok((x, y))
}

fn parse_zone_position(string: &str) -> BoxResult<(i8, i8, i8)> {
    let [x, y, z] = parse_coord_list(string).ok_or("must be x:y:z")?;
    let width = 0..ZONE_WIDTH as i8;
    let height = 0..CHUNK_HEIGHT as i8;
    if !width.contains(&x) || !width.contains(&y) || !height.contains(&z) {
        Err("position in zone is out-of-bounds".into())
    } else {
        Ok((x, y, z))
    }
}

fn parse_inverted_axes(string: &str) -> BoxResult<AxisOrientation> {
    let (x_inverted, y_inverted) = match string {
        "none" => (false, false),
        "x" => (true, false),
        "y" => (false, true),
        "xy" => (true, true),
        _ => return Err("must be one of: none, x, y, xy".into()),
    };

    Ok(AxisOrientation {
        x_inverted,
        y_inverted,
    })
}

fn parse_format_from_name(string: &str) -> image::ImageResult<image::ImageFormat> {
    image::ImageFormat::from_extension(string).ok_or_else(|| {
        image::ImageError::Unsupported(
            image::error::ImageFormatHint::PathExtension(string.into()).into(),
        )
    })
}

fn parse_coord_list<const N: usize, T: FromStr>(string: &str) -> Option<[T; N]> {
    string
        .splitn(N, ':')
        .map(str::parse)
        .collect::<Result<Vec<T>, _>>()
        .ok()?
        .try_into()
        .ok()
}
