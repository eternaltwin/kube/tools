use std::collections::HashMap;

use image::{
    error::ImageFormatHint, EncodableLayout as _, ExtendedColorType, ImageEncoder, ImageError, ImageFormat, ImageResult, Pixel,
    RgbImage,
};
use kube_core::{
    block::Block,
    minimap::HeightMap,
    region::{ChunkDim, ChunkPos, RegionStorage, CHUNK_HEIGHT, CHUNK_WIDTH, ZONE_WIDTH},
};

const ZONES_PER_CHUNK: i32 = kube_core::region::ZONES_PER_CHUNK as i32;

#[derive(Copy, Clone, Debug)]
pub struct ForumPosition {
    pub zone_x: i32,
    pub zone_y: i32,
    pub kube_x: u8,
    pub kube_y: u8,
    pub kube_z: u8,
}

pub fn find_forums_in_chunk(
    pos: ChunkPos,
    chunk: &RegionStorage<Block, ChunkDim>,
) -> Vec<ForumPosition> {
    let forums_per_zone = chunk
        .blocks()
        .filter(|(_, block)| **block == Block::Message)
        .map(|((x, y, z), _)| ForumPosition {
            zone_x: pos.0 * ZONES_PER_CHUNK + (x / ZONE_WIDTH) as i32,
            zone_y: pos.1 * ZONES_PER_CHUNK + (y / ZONE_WIDTH) as i32,
            kube_x: (x % ZONE_WIDTH) as u8,
            kube_y: (y % ZONE_WIDTH) as u8,
            kube_z: (z % CHUNK_HEIGHT) as u8,
        })
        .map(|forum| ((forum.zone_x, forum.zone_y), forum))
        .collect::<HashMap<_, _>>();

    let mut forums: Vec<_> = forums_per_zone.values().copied().collect();
    forums.sort_by_key(|e| (e.zone_x, e.zone_y));
    forums
}

pub fn render_minimaps_with_forums(
    forums: impl Iterator<Item = ForumPosition>,
    chunk_pos: ChunkPos,
    chunk: &RegionStorage<Block, ChunkDim>,
    orientation: kube_core::minimap::AxisOrientation,
    image_format: ImageFormat,
) -> impl Iterator<Item = (ForumPosition, ImageResult<Vec<u8>>)> {
    const SIZE: u32 = CHUNK_WIDTH as u32;
    const COLOR_TYPE: ExtendedColorType = ExtendedColorType::Rgb8;
    let mut minimap = RgbImage::new(SIZE, SIZE);
    let heightmap = HeightMap::from_region(chunk);

    heightmap.render(&mut minimap, 0, 0, orientation);

    forums.map(move |forum| {
        mark_forum_on_chunk_minimap(forum, chunk_pos, &mut minimap, orientation, |minimap| {
            let mut buf: Vec<u8> = Vec::new();
            let result = match image_format {
                ImageFormat::Png => image::codecs::png::PngEncoder::new(&mut buf).write_image(
                    minimap.as_bytes(),
                    minimap.width(),
                    minimap.height(),
                    COLOR_TYPE,
                ),
                _ => Err(ImageError::Unsupported(
                    ImageFormatHint::Exact(image_format).into(),
                )),
            };

            (forum, result.map(|()| buf))
        })
    })
}

fn mark_forum_on_chunk_minimap<T>(
    pos: ForumPosition,
    chunk_pos: ChunkPos,
    minimap: &mut RgbImage,
    orientation: kube_core::minimap::AxisOrientation,
    f: impl FnOnce(&mut RgbImage) -> T,
) -> T {
    let x =
        (pos.zone_x * ZONE_WIDTH as i32) - (chunk_pos.0 * CHUNK_WIDTH as i32) + pos.kube_x as i32;
    let y =
        (pos.zone_y * ZONE_WIDTH as i32) - (chunk_pos.1 * CHUNK_WIDTH as i32) + pos.kube_y as i32;
    if x < 0 || y < 0 || x as u32 >= minimap.width() || y as u32 >= minimap.height() {
        // Out of bounds, nothing to do.
        return f(minimap);
    }

    let x = if orientation.x_inverted {
        minimap.width() as i32 - x
    } else {
        x
    };
    let y = if orientation.y_inverted {
        minimap.height() as i32 - y
    } else {
        y
    };

    invert_colors_in_arrow_pointing_to(minimap, x, y);
    let result = f(&mut *minimap);

    // Restore old colors.
    invert_colors_in_arrow_pointing_to(minimap, x, y);
    result
}

fn invert_colors_in_arrow_pointing_to(img: &mut RgbImage, x: i32, y: i32) {
    const ARROW_HEAD_HALF_WIDTH: i32 = 7;
    const ARROW_SHAFT_HALF_WIDTH: i32 = 3;
    const ARROW_LENGTH: i32 = 32;

    #[derive(Debug)]
    enum Dir {
        Left,
        Right,
        Up,
        Down,
    }
    fn choose_arrow_direction(x: i32, y: i32, width: i32, height: i32) -> Dir {
        if y < ARROW_HEAD_HALF_WIDTH {
            Dir::Up
        } else if y >= height - ARROW_HEAD_HALF_WIDTH {
            Dir::Down
        } else if x <= std::cmp::max(ARROW_LENGTH, width / 2) {
            Dir::Left
        } else {
            Dir::Right
        }
    }

    if x < 0 || y < 0 {
        return;
    }

    img.get_pixel_mut(x as u32, y as u32).invert();

    let dir = choose_arrow_direction(x, y, img.width() as i32, img.height() as i32);

    let mut draw = |i: i32, j: i32| {
        let (px, py) = match dir {
            Dir::Left => (x + i, y + j),
            Dir::Right => (x - i, y - j),
            Dir::Up => (x - j, y + i),
            Dir::Down => (x + j, y - i),
        };
        if px >= 0 && px < img.width() as i32 && py >= 0 && py < img.height() as i32 {
            img.get_pixel_mut(px as u32, py as u32).invert()
        }
    };

    // Arrow shaft
    for i in (2 + ARROW_HEAD_HALF_WIDTH + 1)..(2 + ARROW_LENGTH) {
        for j in (-ARROW_SHAFT_HALF_WIDTH)..=(ARROW_SHAFT_HALF_WIDTH) {
            draw(i, j);
        }
    }

    // Arrow head
    for i in 0..=(ARROW_HEAD_HALF_WIDTH) {
        for j in -i..=i {
            draw(i + 2, j);
        }
    }
}
