use chrono::{DateTime, Utc};

use kube_core::block::Block;
use kube_core::region::{Dimensions, DynDim, RegionStorage, ZONES_PER_CHUNK, ZONE_WIDTH};
use kube_db::KubeDb;

use crate::utils::BoxResult;

// TODO: move into kube-core and merge with db::ZoneBounds?
// Invariant: xmin <= xmax && ymin <= ymax
#[derive(Debug, Copy, Clone)]
pub struct ZoneBounds {
    xmin: i32,
    ymin: i32,
    xmax: i32,
    ymax: i32,
}

impl ZoneBounds {
    pub fn from_bounds(xmin: i32, ymin: i32, xmax: i32, ymax: i32) -> Option<Self> {
        if xmin <= xmax && ymin <= ymax {
            Some(Self { xmin, ymin, xmax, ymax })
        } else {
            None
        }
    }

    pub fn from_bounds_inclusive(xmin: i32, ymin: i32, xmax: i32, ymax: i32) -> Option<Self> {
        Self::from_bounds(xmin, ymin, xmax.checked_add(1)?, ymax.checked_add(1)?)
    }

    pub fn from_rect(x: i32, y: i32, width: u32, height: u32) -> Option<Self> {
        let xmax = x.wrapping_add(width as i32);
        let ymax = y.wrapping_add(height as i32);
        // This will return None if the additions overflowed.
        Self::from_bounds(x, y, xmax, ymax)
    }

    pub fn zoom(self, factor: u32) -> Option<Self> {
        let factor = factor.try_into().ok()?;
        Some(Self {
            xmin: self.xmin.checked_mul(factor)?,
            ymin: self.ymin.checked_mul(factor)?,
            xmax: self.xmax.checked_mul(factor)?,
            ymax: self.ymax.checked_mul(factor)?,
        })
    }

    pub fn dezoom(self, factor: u32) -> Self {
        Self {
            xmin: Self::div_signed(self.xmin, factor, false),
            ymin: Self::div_signed(self.ymin, factor, false),
            xmax: Self::div_signed(self.xmax, factor, true),
            ymax: Self::div_signed(self.ymax, factor, true),
        }
    }

    pub fn xmin(&self) -> i32 {
        self.xmin
    }
    pub fn xmax(&self) -> i32 {
        self.xmax
    }
    pub fn ymin(&self) -> i32 {
        self.ymin
    }
    pub fn ymax(&self) -> i32 {
        self.ymax
    }
    pub fn width(&self) -> u32 {
        // This doesn't overflow, per invariant
        self.xmax.wrapping_sub(self.xmin) as u32
    }
    pub fn height(&self) -> u32 {
        // This doesn't overflow, per invariant
        self.ymax.wrapping_sub(self.ymin) as u32
    }

    #[inline(always)]
    fn div_signed(a: i32, b: u32, round_up: bool) -> i32 {
        let a = i64::from(a);
        let b = i64::from(b);
        let round = if round_up && a % b != 0 { 1 } else { 0 };
        // Cannot overflow because of the original types of a and b.
        (a.div_euclid(b) + round) as i32
    }
}

type Region = RegionStorage<Block, DynDim>;

pub async fn fetch_region_from_db(
    db: &KubeDb,
    zone_bounds: ZoneBounds,
    date: Option<DateTime<Utc>>,
) -> BoxResult<Region> {
    let dims = DynDim::new(
        zone_bounds.width() as usize * ZONE_WIDTH,
        zone_bounds.height() as usize * ZONE_WIDTH,
    );
    let mut region = Region::new_sized(dims);

    // TODO: merge the chunk iteration logic with the one in minimap::render
    let chunk_bounds = zone_bounds.dezoom(ZONES_PER_CHUNK as u32);

    for cy in chunk_bounds.ymin()..chunk_bounds.ymax() {
        for cx in chunk_bounds.xmin()..chunk_bounds.xmax() {
            let chunk = db.get_chunk((cx, cy), date).await?.and_then(|c| c.data);
            if let Some(c) = chunk {
                tokio::task::block_in_place(|| {
                    let chunk = c.into_blocks();

                    let (dst_x, off_x) = get_corner_and_offset(cx, zone_bounds.xmin());
                    let (dst_y, off_y) = get_corner_and_offset(cy, zone_bounds.ymin());
                    let off2_x = std::cmp::min(chunk.dims().width(), dims.width() - dst_x + off_x);
                    let off2_y = std::cmp::min(chunk.dims().depth(), dims.depth() - dst_y + off_y);

                    region.copy_region(
                        &chunk,
                        (dst_x, dst_y, 0),
                        (off_x..off2_x, off_y..off2_y, ..),
                    );
                })
            }
        }
    }

    Ok(region)
}

fn get_corner_and_offset(chunk: i32, zone_min: i32) -> (usize, usize) {
    let zone_min = zone_min as isize;
    let corner = (chunk as isize)
        .checked_mul(ZONES_PER_CHUNK as isize)
        .expect("overflow");
    if corner < zone_min {
        let off = zone_min - corner;
        if off >= ZONES_PER_CHUNK as isize {
            panic!("out-of-bounds chunk");
        }
        (0, off as usize * ZONE_WIDTH)
    } else {
        let pos = ((corner - zone_min) as usize)
            .checked_mul(ZONE_WIDTH)
            .expect("overflow");
        (pos, 0)
    }
}
