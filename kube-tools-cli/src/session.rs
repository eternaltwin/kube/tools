use thiserror::Error;
use twin_network::codec::{CodecKey, DecodeError};
use twin_network::session::TwinSessionId;

// SHA1::encode(",71+")[2..8]
const KUBE_KEY_SALT: &str = "77e352";

pub struct SessionResolver {
    client: reqwest::Client,
}

#[derive(Debug, Error)]
pub enum SessionResolveError {
    #[error("couldn't extract Kube session: session id was rejected")]
    InvalidAuth,
    #[error("couldn't extract Kube FlashVars: {0}")]
    InvalidFlashVars(&'static str),
    #[error("couldn't decode Kube FlashVars: {0}")]
    FlashVarsDecode(#[from] DecodeError),
    #[error("couldn't resolve Kube session: {0}")]
    Request(#[from] reqwest::Error),
}

#[derive(Debug)]
pub struct KubeSession {
    pub data: Vec<u8>,
    pub key: CodecKey,
    pub sid: TwinSessionId,
}

impl SessionResolver {
    pub fn new() -> Result<Self, SessionResolveError> {
        let client = reqwest::ClientBuilder::new()
            .redirect(reqwest::redirect::Policy::none())
            .build()?;
        Ok(Self { client })
    }

    pub async fn resolve(
        &self,
        session_id: &TwinSessionId,
    ) -> Result<KubeSession, SessionResolveError> {
        const KUBE_MAP_URL: &str = "http://kube.muxxu.com/map";

        let resp = self
            .get_url(KUBE_MAP_URL, session_id)
            .send()
            .await?
            .error_for_status()?;
        if resp.status().is_redirection() {
            return Err(SessionResolveError::InvalidAuth);
        }

        let body = resp.text().await?;
        Self::extract_from_body(&body)
    }

    fn extract_from_body(body: &str) -> Result<KubeSession, SessionResolveError> {
        let (swf_name, body) =
            extract_swf_name(body).ok_or(SessionResolveError::InvalidFlashVars("missing swf"))?;
        if swf_name != "/swf/map.swf" {
            return Err(SessionResolveError::InvalidFlashVars("unknwown swf"));
        }

        let (flash_vars, _) = extract_flash_vars(body)
            .ok_or(SessionResolveError::InvalidFlashVars("missing FlashVars"))?;

        let mut data = None;
        let mut key = None;
        let mut sid = None;
        for (name, val) in url::form_urlencoded::parse(flash_vars.as_bytes()) {
            let name = name.strip_prefix("amp;").unwrap_or(&name);

            match name {
                "texts" => (), // ignore
                "infos" => data = Some(val.into_owned()),
                "k" => key = Some(val),
                "sid" => sid = Some(val),
                _ => return Err(SessionResolveError::InvalidFlashVars("unknown FlashVar")),
            }
        }

        let sid =
            TwinSessionId::new(&sid.ok_or(SessionResolveError::InvalidFlashVars("missing sid"))?)
                .map_err(|_| SessionResolveError::InvalidFlashVars("invalid sid"))?;
        let key = key
            .map(|key| CodecKey::new(&key, KUBE_KEY_SALT))
            .ok_or(SessionResolveError::InvalidFlashVars("missing key"))?;
        let data = Self::decode_data(
            &key,
            data.ok_or(SessionResolveError::InvalidFlashVars("missing data"))?,
        )?;

        Ok(KubeSession { data, key, sid })
    }

    fn decode_data(key: &CodecKey, data: String) -> Result<Vec<u8>, SessionResolveError> {
        let mut data = data.into_bytes();
        key.decode(&mut data)?;
        Ok(data)
    }

    fn get_url(&self, url: &str, session_id: &TwinSessionId) -> reqwest::RequestBuilder {
        self.client.get(url).header(
            reqwest::header::COOKIE,
            format!("sid={}", session_id.as_str()),
        )
    }
}

fn extract_swf_name(body: &str) -> Option<(&str, &str)> {
    const SWF_OBJ_START: &str = r#"var so = new js.SWFObject(""#;
    let start = body.find(SWF_OBJ_START)? + SWF_OBJ_START.len();
    let name = &body[start..];
    let end = name.find(|c| c == '"' || c == '?')?;
    Some((&name[..end], &body[start + end + 1..]))
}

fn extract_flash_vars(body: &str) -> Option<(&str, &str)> {
    const FLASH_VARS_START: &str = r#"so.addParam("FlashVars",""#;
    let start = body.find(FLASH_VARS_START)? + FLASH_VARS_START.len();
    let vars = &body[start..];
    let end = vars.find('"')?;
    Some((&vars[..end], &body[start + end + 1..]))
}
