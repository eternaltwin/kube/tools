use std::sync::Arc;

use futures::prelude::*;
use tokio::sync::Semaphore;

use kube_core::msg::{ServerAnswer as KubeAnswer, ServerCmd as KubeCmd};
use twin_network::session::TwinSessionId;
use twin_network::{tora, GameReceiver, GameSender};

use crate::session::KubeSession;

pub struct ConnectOptions<'a> {
    pub url: &'a url::Url,
    pub session: &'a KubeSession,
    pub max_connections: usize,
}

const KUBE_PORT: u16 = 6767;

pub async fn connect(options: ConnectOptions<'_>) -> tora::Result<(KubeReceiver, KubeSender)> {
    use std::io;

    let host = get_host_and_validate_url(options.url).map_err(|_| {
        tora::ToraError::ConnectionError(io::Error::new(
            io::ErrorKind::Other,
            format!("invalid url: {}", options.url),
        ))
    })?;

    let addr = tora::ToraAddr::new(host, KUBE_PORT);
    let (receiver, sender) = twin_network::connect(twin_network::ConnectOptions {
        addr,
        key: options.session.key.clone(),
        sid: options.session.sid.clone(),
        max_connections: options.max_connections,
    })
    .await?;
    Ok((
        KubeReceiver { receiver },
        KubeSender {
            sender,
            endpoint: options.url.path().to_string(),
        },
    ))
}

fn get_host_and_validate_url(url: &url::Url) -> Result<&str, ()> {
    if url.query().is_some() && url.fragment().is_some() {
        return Err(());
    }
    match url.host() {
        Some(url::Host::Domain(host)) => Ok(host),
        _ => Err(()),
    }
}

pub struct KubeSender {
    sender: GameSender,
    endpoint: String,
}

impl KubeSender {
    pub async fn send(&self, cmd: &KubeCmd) -> tora::Result<()> {
        self.sender.send(&self.endpoint, cmd).await
    }

    pub async fn close(self) -> tora::Result<()> {
        self.sender.close().await
    }
}

pub struct KubeReceiver {
    receiver: GameReceiver,
}

impl KubeReceiver {
    pub async fn receive(&mut self) -> tora::Result<Option<KubeAnswer>> {
        self.receiver.receive().await
    }
}

pub struct KubeHtmlOptions<'a> {
    pub session: &'a TwinSessionId,
    pub retry_attempts: usize,
    pub max_connections: usize,
}

#[derive(Clone)]
pub struct KubeHtml {
    client: reqwest::Client,
    retry_attempts: usize,
    conn_semaphore: Arc<Semaphore>,
}

impl KubeHtml {
    const ROOT: &'static str = "http://kube.muxxu.com";

    pub fn new(options: KubeHtmlOptions<'_>) -> reqwest::Result<Self> {
        use reqwest::header::{HeaderMap, HeaderValue};
        let mut headers = HeaderMap::new();
        let cookies = format!("sid={}", options.session.as_str());
        headers.append(
            "COOKIE",
            HeaderValue::from_str(&cookies).expect("invalid cookie headers"),
        );

        let client = reqwest::Client::builder()
            .redirect(reqwest::redirect::Policy::none())
            .pool_max_idle_per_host(options.max_connections)
            .default_headers(headers)
            .build()?;

        Ok(Self {
            client,
            retry_attempts: options.retry_attempts,
            conn_semaphore: Arc::new(Semaphore::new(options.max_connections)),
        })
    }

    fn should_retry(err: &reqwest::Error) -> bool {
        if err.is_timeout() {
            return true;
        }

        let msg = err.to_string();
        // Can spuriously happen if the hyper pool select a closed connection.
        // In this case, we should retry the request with a new connection.
        // See: https://github.com/hyperium/hyper/issues/2136
        // TODO: find a less ugly way to check this
        msg.contains("connection closed before message completed")
    }

    async fn get_raw_response(&self, url: &str) -> reqwest::Result<String> {
        let _guard = self.conn_semaphore.acquire().await;
        let mut retries = self.retry_attempts;
        loop {
            let text = self
                .client
                .get(url)
                .send()
                .and_then(|resp| resp.text())
                .await;

            return match text {
                Ok(text) => Ok(text),
                Err(err) => {
                    if retries > 0 && Self::should_retry(&err) {
                        retries -= 1;
                        continue; // try again
                    } else {
                        Err(err)
                    }
                }
            };
        }
    }

    pub async fn get_zone_info(&self, x: i32, y: i32) -> reqwest::Result<HttpZoneInfo> {
        let url = format!("{}/zone/infos/{}.{}", Self::ROOT, x, y);
        let text = self.get_raw_response(&url).await?;
        Ok(HttpZoneInfo::from_html(&text))
    }
}

#[derive(Debug)]
pub struct HttpZoneInfo {
    pub name: Option<String>,
    pub description: Option<String>,
    pub owner: Option<String>,
    pub owner_id: Option<i32>,
}

impl HttpZoneInfo {
    fn from_html(html: &str) -> Self {
        // Disgusting manual HTML scraping
        fn find_text_inside<'a>(html: &'a str, before: &str, after: &str) -> Option<&'a str> {
            let start = html.find(before)? + before.len();
            let html = &html[start..];
            let end = html.find(after)?;
            Some(&html[..end])
        }

        let name = find_text_inside(html, r#"<li class="name">"#, "</li>").map(String::from);
        let description = find_text_inside(html, r#"<li class="desc">"#, "</li>").map(String::from);
        let owned_text = find_text_inside(html, r#"<li class="user" class="rentedBy">"#, "</li>");
        let (owner, owner_id) = match owned_text {
            Some(txt) => {
                let name = find_text_inside(txt, r#"">"#, "</a>").map(String::from);
                let id = find_text_inside(txt, r#"<a href="/user/"#, "\"")
                    .and_then(|id| id.parse().ok());
                (name, id)
            }
            None => (None, None),
        };
        HttpZoneInfo {
            name,
            description,
            owner,
            owner_id,
        }
    }
}
