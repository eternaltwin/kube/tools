
-- Add `owner_id` and `description` columns to `zones` table

ALTER TABLE zones ADD COLUMN owner_id INTEGER;
ALTER TABLE zones ADD COLUMN description TEXT;
