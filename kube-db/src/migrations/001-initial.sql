
-- Initial migration: create empty tables

CREATE TABLE chunks (
    mx INTEGER NOT NULL,
    my INTEGER NOT NULL,
    data BYTEA,
    created_at TIMESTAMPTZ NOT NULL,

    PRIMARY KEY (mx, my)
);

CREATE TABLE zones (
    x INTEGER NOT NULL,
    y INTEGER NOT NULL,
    location_end TIMESTAMPTZ,
    owner VARCHAR(100),
    name VARCHAR(100),
    public BOOLEAN NOT NULL,
    created_at TIMESTAMPTZ NOT NULL,

    PRIMARY KEY (x, y)
);
