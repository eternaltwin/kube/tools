
-- Add history tables for `chunks` and `zones`

CREATE TABLE chunks_history (
    mx INTEGER NOT NULL,
    my INTEGER NOT NULL,
    data BYTEA,
    compressed BOOLEAN NOT NULL,
    is_delta BOOLEAN NOT NULL,
    valid_since TIMESTAMPTZ NOT NULL,

    PRIMARY KEY (mx, my, valid_since),
    CONSTRAINT chk_compressed CHECK (data IS NOT NULL OR compressed),
    CONSTRAINT chk_delta CHECK (data IS NOT NULL OR NOT is_delta)
);

CREATE TABLE zones_history (
    x INTEGER NOT NULL,
    y INTEGER NOT NULL,
    location_end TIMESTAMPTZ,
    owner VARCHAR(100),
    owner_id INTEGER,
    name VARCHAR(100),
    description TEXT,
    valid_range TSTZRANGE NOT NULL,

    PRIMARY KEY (x, y, valid_range),
    EXCLUDE USING GIST (x WITH =, y WITH =, valid_range WITH &&)
);

-- Add `last_checked_at` column to `chunks`

ALTER TABLE chunks ADD COLUMN last_checked_at TIMESTAMPTZ;
UPDATE chunks SET last_checked_at = created_at;
ALTER TABLE chunks ALTER COLUMN last_checked_at SET NOT NULL;
ALTER TABLE chunks ADD CONSTRAINT chk_times CHECK (
    created_at <= last_checked_at
);

-- Add `chunks_full_history` and `zones_full_history` views

CREATE VIEW chunks_full_history AS (
    SELECT * FROM chunks_history UNION ALL
    SELECT
        chunks.mx, chunks.my,
        chunks.data, chunks.compressed,
        FALSE as is_delta,
        chunks.created_at as valid_since
    FROM chunks
);

CREATE VIEW zones_full_history AS (
    SELECT * FROM zones_history UNION ALL
    SELECT 
        zones.x, zones.y,
        zones.location_end,
        zones.owner, zones.owner_id,
        zones.name, zones.description,
        TSTZRANGE(zones.created_at, NULL) as valid_range
    FROM zones
);
