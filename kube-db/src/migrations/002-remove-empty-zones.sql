
-- Modify `zones` table to only track zones with location

-- Delete empty zones
DELETE FROM zones WHERE (owner IS NULL AND NOT public);
-- Make sure that public zones have no owner
UPDATE zones SET owner = NULL WHERE public;
-- Remove `public` column
ALTER TABLE zones DROP COLUMN public;
