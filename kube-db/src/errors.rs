use chrono::{DateTime, Utc};
use thiserror::Error;

use crate::migrations::CURRENT_SCHEMA_VERSION;

#[derive(Debug, Error)]
pub enum DbOpenError {
    #[error(transparent)]
    DbError(#[from] sqlx::Error),
    #[error("couldn't determine database schema")]
    UnknownSchema,
    #[error(
        "database schema v{0} is unsupported (expected v{})",
        CURRENT_SCHEMA_VERSION
    )]
    UnsupportedSchema(i32),
    #[error("error while running migrations: {0}")]
    MigrationError(#[source] sqlx::Error),
}

#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    Sqlx(#[from] sqlx::Error),
    #[error("error while decoding chunk: {0}")]
    Chunk(#[from] kube_core::region::RegionError),
    #[error("tried to save outdated data: {}, database has {}", .outdated, .saved)]
    OutdatedData {
        outdated: DateTime<Utc>,
        saved: DateTime<Utc>,
    },
}

pub type Result<T> = std::result::Result<T, Error>;
